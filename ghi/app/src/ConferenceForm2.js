import React, { useEffect, useState } from "react";
import FormField from "./FormField.tsx";

function ConferenceForm() {
  // |---------------------|
  // |        state        |
  // |---------------------|

  const [name, setName] = useState("");
  const [maxPresentations, setMaxPresentations] = useState(0);
  const [maxAttendees, setMaxAttendees] = useState(0);
  const [StartDate, setStartDate] = useState(null);
  const [EndDate, setEndDate] = useState(null);
  const [description, setDescription] = useState("");
  const [location, setLocation] = useState("");
  const [locations, setLocations] = useState([]);

  const resetStates = () => {
    setName("");
    setMaxPresentations(0);
    setMaxAttendees(0);
    setStartDate(null);
    setEndDate(null);
    setLocation("");
    setDescription("");
  };

  const Fields = [
    ["name", "text",name, handleNameChange],
    ["maxPresentations", "number", maxPresentations,handlePresentationsChange],
    ["maxAtendees", "number",maxAttendees, handleAtendeesChange],
    ["starts", "date", StartDate,handleStartDateChange],
    ["ends", "date", EndDate, handleEndDateChange],
  ];

  // |-------------------------------|
  // |        form submission        |
  // |-------------------------------|
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.description = description;
    data.max_presentations = maxPresentations;
    data.max_attendees = maxAttendees;
    data.starts = StartDate;
    data.ends = EndDate;
    if (location){
        data.location = location.match(/\d/)[0];
    } else {
        data.location = 1
    }

    console.log(data)

    const locationUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);
      resetStates()
    //   setName("");
    //   setMaxPresentations(0);
    //   setMaxAttendees(0);
    //   setStartDate(null);
    //   setEndDate(null);
    //   setLocation("");
    //   setDescription("");
    }
  };


  // |--------------------------------|
  // |        handle functions        |
  // |--------------------------------|

  function handleNameChange(event) {
    setName(event.target.value);
  }

  function handlePresentationsChange(event) {
    setMaxPresentations(event.target.value);
  }

  function handleAtendeesChange(event) {
    setMaxAttendees(event.target.value);
  }

  function handleStartDateChange(event) {
    setStartDate(event.target.value);
  }

  function handleEndDateChange(event) {
    setEndDate(event.target.value);
  }

  function handleDescriptionChange(event) {
    setDescription(event.target.value);
  }

  // |-------------------------|
  // |        load data        |
  // |-------------------------|

  const fetchData = async () => {
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  // |-------------------------|
  // |        component        |
  // |-------------------------|

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Conference</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            {Fields.map(([name, type, value, onChange]) => {
              return (
                <FormField
                  key={name}
                  name={name}
                  type={type}
                  value={value}
                  onChange={onChange}
                />
              );
            })}

            <div className="mb-3">
              <label htmlFor="description" className="form-label">
                Description
              </label>
              <textarea
                required
                onChange={handleDescriptionChange}
                className="form-control"
                name="description"
                id="description"
                rows="3"
              ></textarea>
            </div>
            <div className="mb-3">
              <select
                onChange={(event) => setLocation(event.target.value)}
                required
                id="location"
                name="location"
                className="form-select"
              >
                <option value="">Choose a location</option>
                {locations.map((location) => {
                  return (
                    <option key={location.name} value={location.href}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
