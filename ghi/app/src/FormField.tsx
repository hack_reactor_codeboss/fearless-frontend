import React from "react";

//  { name, type, onChange }
interface Props {
  name: string,
  type: string,
  value: string | number,
  onChange: ()=> void,
}


function FormField({name, type, value, onChange}:Props) {
  return (
    <div className="form-floating mb-3">
      <input
        onChange={onChange}
        placeholder={name}
        type={type}
        name={name}
        value={value}
        id={name}
        className="form-control"
      />
      <label htmlFor={name}>{name}</label>
    </div>
  );
}

export default FormField;
