import React, { useState, useEffect } from "react";

function PresentationForm() {
  const [conferences, setConferences] = useState([]);
  const [conference,setConference] = useState("")
  const [formData, setFormData] = useState({
    presenter_name: "",
    company_name: "",
    presenter_email: "",
    title: "",
    synopsis: "",
  });


  // |-------------------------|
  // |        load data        |
  // |-------------------------|

  async function fetchData() {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
      console.log(conference)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);



  // |---------------------------|
  // |        submit data        |
  // |---------------------------|
  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log("submiting data now")
    console.log("here is current data", formData)
    console.log("here is current conference", conference)


    const url = `http://localhost:8000/api/conferences/${conference}/presentations/`;

    console.log("here is conference url", url)
    const json = JSON.stringify(formData)
    console.log(json)
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      const data = response.json()
      console.log(data)
      setFormData({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
      });
    }else {
      console.log("bad response")
      console.log(response)
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  }


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit}id="create-presentation-form">
            <div className="form-floating mb-3">
              <input
                placeholder="Presenter name"
                onChange={handleFormChange}
                required
                type="text"
                name="presenter_name"
                id="presenter_name"
                className="form-control"
                />
              <label htmlFor="presenter_name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Presenter email"
                required
                type="email"
                name="presenter_email"
                id="presenter_email"
                className="form-control"
                />
              <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Company name"
                type="text"
                name="company_name"
                id="company_name"
                className="form-control"
                />
              <label htmlFor="company_name">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Title"
                required
                type="text"
                name="title"
                id="title"
                className="form-control"
                />
              <label htmlFor="title">Title</label>
            </div>
            <div className="mb-3">
              <label htmlFor="synopsis">Synopsis</label>
              <textarea
                onChange={handleFormChange}
                className="form-control"
                id="synopsis"
                rows="3"
                name="synopsis"
                ></textarea>
            </div>
            <div className="mb-3">
              <select
                required
                onChange={(event)=> setConference(event.target.value)}
                name="conference"
                id="conference"
                className="form-select"
              >
                <option value="">
                  Choose a conference
                </option>
                {conferences.map(conference => {
                    return (
                        <option key={conference.id} value={conference.id}>{conference.name}</option>
                    )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
