function createCard(dates, name, description, pictureUrl,location) {
  return `
    <div class="card shadow-lg mb-4">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer text-muted">
      <p> ${dates.starts} - ${dates.ends}</p>
    </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      document.querySelector('.row').innerHTML = createError('conference response is bad')

    } else {
      const data = await response.json();
      for (let i = 0; i < data.conferences.length; i++) {
        const conference = data.conferences[i]

        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts).toLocaleDateString()
          const endDate = new Date(details.conference.ends).toLocaleDateString()
          const location = details.conference.location.name
          const html = createCard({starts: startDate, ends: endDate},title, description, pictureUrl,location);
          const col_idx = i % 3
          const columns = document.querySelectorAll('.col');
          columns[col_idx].innerHTML += html;
        } // if detail response ok
      } // for

    } // if / else
  } catch (e) {
    // Figure out what to do if an error is raised
    document.querySelector('.row').innerHTML = createError(e)

  }

});


function createError(message){
  return `
  <div class="alert alert-primary" role="alert">
  <p>${message}</p>
  </div>
  `;
} // createError
