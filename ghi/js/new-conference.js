
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      for (let location of data.locations){
        const locationOptions = document.getElementById("location")
        const newOpt = document.createElement("option")
        newOpt.value = location.id
        newOpt.innerHTML = location.name
        locationOptions.appendChild(newOpt)
      } // for
    } // response ok


    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
        }; // fetch config
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        }



    });

  });
