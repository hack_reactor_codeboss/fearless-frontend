
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      for (let state of data.states){
        const stateOptions = document.getElementById("state")
        const newOpt = document.createElement("option")

        newOpt.value = state.abbreviation
        newOpt.innerHTML = state.name
        stateOptions.appendChild(newOpt)
      } // for
    } // response ok


    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
        }; // fetch config
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        }



    });

  });
