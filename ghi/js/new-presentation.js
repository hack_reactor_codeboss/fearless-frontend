
window.addEventListener('DOMContentLoaded', async () => {


// |--------------------------------------------------------------------|
// |            add conferences to the new presentation form            |
// |--------------------------------------------------------------------|

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      for (let conference of data.conferences){
        const conferenceOptions = document.getElementById("conference")
        const newOpt = document.createElement("option")
        newOpt.value = conference.href
        newOpt.innerHTML = conference.name
        conferenceOptions.appendChild(newOpt)
      } // for
    } // response ok



    // |---------------------------------------------------|
    // |        POST completed form to database API        |
    // |---------------------------------------------------|

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = Object.fromEntries(new FormData(formTag));

        const json = JSON.stringify(formData);

        const locationUrl = `http://localhost:8000${formData.conference}presentations/`;
        const fetchConfig = {
        method: "post",
        body: json,docke
        headers: {
            'Content-Type': 'application/json',
        },
        }; // fetch config
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        }



    });

  });
